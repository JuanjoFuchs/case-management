import './index.css'
import 'sugar'
import React from 'react'
import App from './components/app'
import { setCaseManager, loadMyCases } from './actions'
import { render } from 'react-dom'

setCaseManager()
loadMyCases()
render(<App />, document.getElementById('app'))
