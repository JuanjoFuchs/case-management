import state from './state'

let rootUrl = ''

if (process.env.NODE_ENV != 'production') {
  rootUrl = 'https://lm.cignium.com/run/cignium/cm/dev/'
}

export function setStatus(status) {
  localStorage.setItem('selectedStatus', status)
  state.get().set('selectedStatus', status)
}

export function loadFollowUps(caseId) {
  fetch(`${rootUrl}list-followups?caseid=${caseId}`)
    .then(response => response.json())
    .then(json => state.get().set('followUps', json))
}

export function loadMyCases() {
  const caseManagerId = 'c2e336b7-fc99-4277-a3e2-b37afa800fe1'
  //loadData(`my-cases?CaseManagerID=${caseManagerId}`, 'myCases')
  state.get().set('myCases', [
    {
      'Id': '1',
      'Case Manager ID': caseManagerId,
      'Customer': {},
      'Insurance Policy': {
        'Insurance Product': {},
      },
    },
  ])
}

export function navigateFollowUp(followUp) {
  Cignium.navigate(`${rootUrl}update-followup?followupid=${followUp['FollowupId']}`)
}

export function generateTestCase() {
  const formData = new FormData()
  formData.append('Case Manager ID', '1')
   
  fetch(`${rootUrl}add-case`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formData,
  }).then(this.loadMyCases())
}

export function loadMyReminders() {
  const caseManagerId = 'c2e336b7-fc99-4277-a3e2-b37afa800fe1'
  loadData(`reminders?CaseManagerID=${caseManagerId}`, 'myReminders')
}

export function updateCase(myCase) {
  Cignium.navigate(`${rootUrl}updatecase?id=${myCase['Id']}`)
}

export function setCaseManager() {
  state.get().set('caseManager', {
    'Id':'c2e336b7-fc99-4277-a3e2-b37afa800fe1',
    'Name':'Diego Torres',
    'Email':'diego.torres@cignium.com',
  })
}

function loadData(url, prop) {
  return fetch(`${rootUrl}${url}`)
    .then(response => response.json())
    .then(json => state.get().set(prop, json))
}
