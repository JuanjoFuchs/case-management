import Freezer from 'freezer-js'

export default new Freezer({
  followUps: [],
  myCases: [],
  myReminders: [],
  caseManager: localStorage.getItem('caseManager'),
  selectedStatus: localStorage.getItem('selectedStatus'),
})
