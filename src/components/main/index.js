import React, { Component } from 'react'
import styles from './index.css'
import { loadFollowUps, updateCase, navigateFollowUp } from '../../actions'
import CaseList from './caseList'

export default class Main extends Component {
  componentDidMount() {
    Cignium.init(this.clientRenderer)
  }

  render() {
    const { followUps, myCases, title } = this.props
    let selectedStatus = this.props.selectedStatus

    if (myCases.none(c => c['Case Status'] == selectedStatus)) {
      selectedStatus = null
    }
    
    const content = (<CaseList cases={myCases} selectedStatus={selectedStatus} />) 

    return (
      <div className={styles.root}>
        <div className={styles.header}>
          Case Management
        </div>
        <div className={styles.main}>
          <div className={styles.navigator}>
            {content}
          </div>
          <div className={styles.content} ref={ref => this.clientRenderer = ref}>
            launch process
          </div>
        </div>
      </div>
    )
  }
}
