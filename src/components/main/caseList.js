import React, { Component } from 'react'
import styles from './index.css'

export default ({ cases, selectedStatus }) => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Carrier</th>
        <th>Product Line</th>
        <th>Coverage Amount</th>
        <th>Annual Premium</th>
        <th>Application Date</th>
        <th></th>
      </tr>
    </thead>
    {cases.filter(myCase => !selectedStatus || myCase['Case Status'] == selectedStatus).map(myCase => (
      <tbody key={myCase['Case Id']}>
        <tr>
          <td>
            {myCase['Customer']['First Name'] + ' '}
            {myCase['Customer']['Last Name']}
          </td>
          <td>
            {myCase['Insurance Policy']['Insurance Product']['Carrier']}
          </td>
          <td>
            {myCase['Insurance Policy']['Insurance Product']['Product Line']}
          </td>
          <td>
            {myCase['Insurance Policy']['Insurance Product']['Coverage Amount']}
          </td>
          <td>
            {myCase['Insurance Policy']['Insurance Product']['Annual Premium']}
          </td>
          <td>
            {new Date(myCase['Insurance Policy']['Application Date']).toLocaleDateString()}
          </td>
          <td>
            <button
              className={styles.action}
              onClick={() => updateCase(myCase)}>
              Update Case
            </button>
          </td>
        </tr>
      </tbody>
      ))}
  </table>
)
