import React from 'react'
import Group from './group'
import Profile from './profile'
import Reminders from './reminders'
import styles from './index.css'
import { generateTestCase } from '../../actions'

export default ({ myCases, myReminders, selectedStatus, caseManager }) => (
  <div className={styles.root}>
    <Profile name={caseManager['Name']} />
    <Group items={groupCasesByStatus(myCases)} selectedStatus={selectedStatus} title='My Cases' />
    <Reminders reminders={myReminders} />
  </div>
)

function groupCasesByStatus(cases) {
  const statuses = cases.exclude(c => !c['Case Status']).groupBy('Case Status')

  return Object.keys(statuses).map(status => ({
    items: statuses[status],
    title: status,
  }))
}
