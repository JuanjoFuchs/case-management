import React from 'react'
import Reminder from './reminder'
import styles from './group.css'

export default ({ reminders }) => (
  <div className={styles.root}>
    <div className={styles.header}>
      <div className={styles.headerText}>
        My Reminders
      </div>
      <div className={styles.count}>
        {reminders.length}
      </div>
    </div>
    {reminders.map(reminder => <Reminder reminder={reminder} />)}
  </div>
)
