import React from 'react'
import cx from 'classnames'
import styles from './item.css'
import { setStatus } from '../../actions'

export default ({ item, selectedStatus }) => (
  <div
    className={cx(styles.root, { [styles.selected]: item.title == selectedStatus })}
    onClick={() => setStatus(selectedStatus == item.title ? null : item.title)}>
    <div className={styles.main}>
      {item.title}
    </div>
    <div className={styles.count}>
      {item.items.length}
    </div>
  </div>
)
