import React from 'react'
import styles from './profile.css'

export default ({ name }) => (
  <div className={styles.root}>
    <div className={styles.avatar} />
    <div className={styles.name}>
      {name}
    </div>
  </div>
)
