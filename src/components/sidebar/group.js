import React from 'react'
import Item from './item'
import styles from './group.css'

export default ({ items, selectedStatus, title }) => (
  <div className={styles.root}>
    <div className={styles.header}>
      <div className={styles.headerText}>
        {title}
      </div>
      <div className={styles.count}>
        {items.sum(item => item.items.length)}
      </div>
    </div>
    {items.map(item => <Item key={item.title} item={item} selectedStatus={selectedStatus} />)}
  </div>
)
