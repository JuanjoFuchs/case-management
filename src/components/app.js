import React, { Component } from 'react'
import Main from './main/index'
import Sidebar from './sidebar/index'
import Login from './login/index'
import state from '../state'
import styles from './app.css'

export default class App extends Component {
  constructor() {
    super()
    this.state = state.get()
  }

  componentDidMount() {
    state.on('update', state => this.setState(state))
  }

  render() {
    const { followUps, myCases, myReminders, selectedStatus, caseManager } = this.state
    
    if (caseManager) {
      return (
        <div className={styles.root}>
          <Sidebar myCases={myCases} myReminders={myReminders} selectedStatus={selectedStatus} caseManager={caseManager} />
          <Main followUps={followUps} myCases={myCases} selectedStatus={selectedStatus} />
        </div>
      )
    }
    
    return <Login />
  }
}
