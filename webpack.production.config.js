import shared from './webpack.shared'
import webpack from 'webpack'

export default {
  ...shared,
  entry: './src',
  output: {
    filename: './dist/app.js',
    libraryTarget: 'var',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new webpack.optimize.UglifyJsPlugin({
      comments: false,
      compress: {
        warnings: false,
      },
    }),
  ],
}
