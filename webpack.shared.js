export default {
  module: {
    loaders: [{
      include: /src/,
      loaders: ['babel', 'eslint'],
      test: /\.js$/,
    },{
      loaders: ['style', 'css?modules'],
      test: /\.css$/,
    }],
  },
}
