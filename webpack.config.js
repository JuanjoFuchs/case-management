import shared from './webpack.shared'
import webpack from 'webpack'

export default {
  ...shared,
  devtool: 'source-map',
  entry: [
    'webpack-hot-middleware/client',
    './src',
  ],
  output: {
    filename: 'app.js',
    path: __dirname,
    publicPath: 'http://localhost:3000/',
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ],
}
