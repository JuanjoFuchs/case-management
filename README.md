# Case Management Shell

## Get Started

1. Install [NodeJS](https://nodejs.org/en/download/)
1. Run ```npm install```
2. Run ```npm run start```
4. Navigate to [http://localhost:3001](http://localhost:3001/)
